---
- name: Check if slapd olc directory exists.
  stat:
    path: "{{ slapd_etc_dir }}/slapd.d"
  register: st_slapd_configdir

- name: Check if slapd db directory has content after initial package installation.
  stat:
    path: "{{ slapd_data_dir }}/DB_CONFIG.example"
  register: st_slapd_dbdir

- name: Provision the OpenLDAP server from scratch.
  when:
    - not st_slapd_configdir.stat.exists
    - st_slapd_dbdir.stat.exists
  block:
  - name: Ensure slapd is stopped.
    service:
      name: "slapd"
      state: "stopped"

  - name: Remove files/dirs not needed.
    file:
      name: "{{ item }}"
      state: "absent"
    loop: "{{ slapd_files_remove }}"

  - name: Copy custom LDAP schema files.
    copy:
      src: "usr/local/etc/ldap/schema/"
      dest: "{{ slapd_etc_dir }}/schema/"
      owner: "root"
      group: "root"
      mode: "0644"

  - name: Template initial slapd config.
    template:
      src: "tmp/initial_slapd_config.ldif.j2"
      dest: "/tmp/initial_slapd_config.ldif"

  - name: Ensure OLC directory is present.
    file:
      path: "{{ slapd_etc_dir }}/slapd.d"
      state: "directory"
      owner: "{{ slapd_user }}"
      group: "{{ slapd_group }}"
      mode: "0755"

  - name: Ensure OpenLDAP database directory is present.
    file:
      path: "{{ slapd_data_dir }}"
      state: "directory"
      owner: "{{ slapd_user }}"
      group: "{{ slapd_group }}"
      mode: "0700"

  - name: Create initial Online-Configuration Database.
    shell: "/usr/local/openldap/sbin/slapadd -F {{ slapd_etc_dir }}/slapd.d -b 'cn=config' -l /tmp/initial_slapd_config.ldif"

  - name: Ensure OLC directory is present.
    file:
      path: "{{ slapd_etc_dir }}/slapd.d"
      state: "directory"
      owner: "{{ slapd_user }}"
      group: "{{ slapd_group }}"

  - name: Cleanup helper files.
    file:
      path: "{{ item }}"
      state: "absent"
    loop:
      - "/tmp/initial_slapd_config.ldif"
      - "/usr/sbin/policy-rc.d"
